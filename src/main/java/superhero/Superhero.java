package superhero;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "superhero")
public class Superhero {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Pattern(regexp = "^(?! )[A-Za-z0-9., ]*(?<! )$")
    @Length(min = 1, max = 255)
    private String name;

    @Pattern(regexp = "^(?! )[A-Za-z0-9., ]*(?<! )$")
    @Length(min = 1, max = 255)
    private String pseudonym;

    @Pattern(regexp = "^(DC|Marvel)$")
    private String publisher;

    @EachPattern(regexp = "^(?! )[A-Za-z0-9., ]*(?<! )$")
    @Transient
    private List<String> powers;

    @Column(name = "powers")
    private String powersColumn;

    @EachPattern(regexp = "^(?! )[A-Za-z0-9., ]*(?<! )$")
    @Transient
    private List<String> allies;

    @Column(name = "allies")
    private String alliesColumn;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private String firstPublished;

    public Superhero(@JsonProperty("name") String name,
                     @JsonProperty("pseudonym") String pseudonym,
                     @JsonProperty("publisher") String publisher,
                     @JsonProperty("powers") List<String> powers,
                     @JsonProperty("allies") List<String> allies,
                     @JsonProperty("firstPublished") String firstPublished){
        this.name = name;
        this.pseudonym = pseudonym;
        this.publisher = publisher;
        this.powers = powers;
        this.allies = allies;
        this.firstPublished = firstPublished;

        updateLists();
    }

    public Superhero(){
        this.name = null;
        this.pseudonym = null;
        this.publisher = null;
        this.powers = null;
        this.allies = null;
        this.firstPublished = null;

        this.powersColumn = null;
        this.alliesColumn = null;
    }

    public String getName() {
        return name;
    }

    public String getPseudonym() {
        return pseudonym;
    }

    public String getPublisher() {
        return publisher;
    }

    public List<String> getPowers() {
        return powers;
    }

    public List<String> getAllies() {
        return allies;
    }

    public String getFirstPublished() {
        return firstPublished;
    }

    public void updateLists() {
        if(powers != null){
            powersColumn = String.join(",", powers);
        }
        else if(powersColumn != null){
            powers = Arrays.asList(powersColumn.split(","));
        }

        if(allies != null){
            alliesColumn = String.join(",", allies);
        }
        else if(alliesColumn != null){
            allies = Arrays.asList(alliesColumn.split(","));
        }
    }

}
