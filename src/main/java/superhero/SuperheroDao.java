package superhero;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
public interface SuperheroDao extends CrudRepository<Superhero,Long>{
    public Optional<Superhero> findByPseudonym(String pseudonym);

    public Optional<Superhero> findByName(String name);

    @Query("SELECT s.pseudonym FROM Superhero s")
    public List<String> getPseudonymList();
}
