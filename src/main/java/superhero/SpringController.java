package superhero;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;
import java.sql.SQLException;
import java.util.*;

@RestController
public class SpringController {

    private SuperheroDao superheroDao;

    @Autowired
    SpringController(SuperheroDao superheroDao){
        this.superheroDao = superheroDao;
    }

    @RequestMapping("/list-superheroes")
    public @ResponseBody List<String> listSuperheroes() throws SQLException{
        return superheroDao.getPseudonymList();
    }

    @RequestMapping("/get-superhero")
    public ResponseEntity<Superhero> test(@RequestBody Superhero superhero) throws SQLException {
        final String name = superhero.getName();
        final String pseudonym = superhero.getPseudonym();
        Optional<Superhero> responseSuperhero;

        if((name != null && pseudonym != null) || (name == null && pseudonym == null)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if(name != null){
            responseSuperhero = superheroDao.findByName(name);
        }
        else {
            responseSuperhero = superheroDao.findByPseudonym(pseudonym);
        }
        if(responseSuperhero.isPresent()){
            responseSuperhero.get().updateLists();
            return new ResponseEntity<>(responseSuperhero.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping("/add-superhero")
    public ResponseEntity<String> addSuperhero(@RequestBody @Valid Superhero superhero) throws SQLException {
        superhero.updateLists();

        if(superheroDao.findByName(superhero.getName()).isPresent()){
            return new ResponseEntity<>("A superhero with the same name is already in the DB.", HttpStatus.BAD_REQUEST);
        }
        if(superheroDao.findByPseudonym(superhero.getPseudonym()).isPresent()){
            return new ResponseEntity<>("A superhero with the same pseudonym is already in the DB.", HttpStatus.BAD_REQUEST);
        }

        superheroDao.save(superhero);
        return new ResponseEntity<>("added superhero to DB", HttpStatus.OK);
    }

    @RequestMapping("/auth")
    public ResponseEntity<String> auth(@RequestBody User user){
        if(!user.getUsername().equals("root") || !user.getPassword().equals("toor")){
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, 1);
        final Date expirationDate = calendar.getTime();

        String jwtToken = Jwts.builder()
                .setSubject("authorized")
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS256, TokenAuthFilter.key)
                .compact();

        return new ResponseEntity<>(jwtToken,HttpStatus.OK);
    }

}
