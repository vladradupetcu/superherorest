package superhero;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EachPatternValidator implements ConstraintValidator<EachPattern, List<String>> {

    private String regexp;

    @Override
    public void initialize(EachPattern constraintAnnotation){
        regexp = constraintAnnotation.regexp();
    }

    @Override
    public boolean isValid(List<String> object, ConstraintValidatorContext constraintContext){
        Pattern pattern = Pattern.compile(regexp);
        for(String checkString : object){
            Matcher matcher = pattern.matcher(checkString);
            if(!matcher.find()){
                return false;
            }
        }
        return true;
    }
}
