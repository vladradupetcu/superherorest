package superhero;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class TokenAuthFilter extends GenericFilterBean {

    public final static String key = "verysecretkey";

    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterChain) throws IOException, ServletException {
        final HttpServletRequest httpRequest = (HttpServletRequest) request;
        final String authorizationHeader = httpRequest.getHeader("Authorization");

        if(authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")){
            throw new ServletException("Missing or malformed authorization header.");
        }

        final String token = authorizationHeader.substring(7);
        try {
            Jwts.parser().setSigningKey(key).parse(token);
        } catch (SignatureException e) {
            throw new ServletException("Invalid token.");
        }

        filterChain.doFilter(request, response);
    }
}
