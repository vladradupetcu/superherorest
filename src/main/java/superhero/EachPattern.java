package superhero;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;


@Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = EachPatternValidator.class)
@Documented
public @interface EachPattern {
    String message() default "There are non-allowed characters in the input.";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

    String regexp();

    @Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE })
    @Retention(RUNTIME)
    @Documented
    @interface List {
        EachPattern[] regexp();
    }
}
