package superhero;

import org.flywaydb.core.Flyway;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

    private final static String databaseURL = "jdbc:mysql://localhost:6603/superhero";
    private final static String databaseUser = "root";
    private final static String databasePassword = "testpass";

    @Bean
    public FilterRegistrationBean tokenAuthFilter() {
        final FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setFilter(new TokenAuthFilter());
        filterRegistrationBean.addUrlPatterns("/list-superheroes");
        filterRegistrationBean.addUrlPatterns("/get-superheroe");
        filterRegistrationBean.addUrlPatterns("/add-superheroe");

        return filterRegistrationBean;
    }

    public static void main(String[] args){
        //use flyway to initialize db
        Flyway flyway = new Flyway();
        flyway.setDataSource(databaseURL, databaseUser, databasePassword);
        flyway.migrate();

        //boot up spring
        SpringApplication.run(Application.class, args);
    }
}