package superhero;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.NonTypedScalarSerializerBase;
import org.springframework.web.util.HtmlUtils;

import java.io.IOException;

public class SanitizingConverter extends NonTypedScalarSerializerBase<String> {

    public SanitizingConverter(){
        super(String.class);
    }

    @Override
    public void serialize(String value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeRawValue("\"" + HtmlUtils.htmlEscape(value) + "\"");
    }

}
