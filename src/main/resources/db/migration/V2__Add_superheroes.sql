INSERT INTO superhero (name, pseudonym, publisher, powers, allies, first_published) values (
  'Bruce Wayne',
  'Batman',
  'DC',
  'Peak Human',
  'Robin',
  '1970-03-20'
);

INSERT INTO superhero (name, pseudonym, publisher, powers, allies, first_published) values (
  'Clark Kent',
  'Superman',
  'DC',
  'Flying,Superstrength',
  'Sarah Jane',
  '1938-06-01'
);

INSERT INTO superhero (name, pseudonym, publisher, powers, allies, first_published) values (
  'Barry Allen',
  'The Flash',
  'DC',
  'Superspeed',
  'Wally',
  '1940-01-15'
);

