CREATE TABLE superhero(
  id INT(10) NOT NULL AUTO_INCREMENT,
  name VARCHAR(100) NOT NULL,
  pseudonym VARCHAR(100) NOT NULL,
  publisher ENUM('DC','Marvel') NOT NULL,
  powers VARCHAR(255) NOT NULL,
  allies VARCHAR(255) NOT NULL,
  first_published DATE NOT NULL,
  PRIMARY KEY (id)
);