#!/bin/bash

CONTAINER_NAME="mysql-vlad"
MYSQL_PASSWORD="testpass"
MYSQL_DATABASE="superhero"

echo Container name: $CONTAINER_NAME
echo MySQL Password: $MYSQL_PASSWORD
echo MySQL DB: $MYSQL_DATABASE

docker stop $CONTAINER_NAME
docker rm $CONTAINER_NAME
docker run --detach --name=$CONTAINER_NAME -e MYSQL_ROOT_PASSWORD="$MYSQL_PASSWORD" -e MYSQL_DATABASE="$MYSQL_DATABASE" --publish 6603:3306 mysql
